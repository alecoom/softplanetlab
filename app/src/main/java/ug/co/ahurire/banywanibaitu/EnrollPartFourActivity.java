package ug.co.ahurire.banywanibaitu;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.gson.JsonArray;

import java.io.File;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ug.co.ahurire.banywanibaitu.models.User;
import ug.co.ahurire.banywanibaitu.services.ApiService;
import ug.co.ahurire.banywanibaitu.utils.Utils;

public class EnrollPartFourActivity extends AppCompatActivity {
    Button takeIdPhotoBtn, submitBtn;
    Uri mUri;
    private static final int TAKE_PHOTO_ID = 0;
    private static final int REQUEST_READ_PHONE_STATE = 0;
    private Bitmap mPhoto;
    ImageView enrollIdPhoto;
    User user;
    ProgressDialog progressDialog;
    Context context = this;
    RestAdapter registrationAdapter;
    ApiService apiService;
    TelephonyManager telephonyManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll_part_four);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(context);
        registrationAdapter = new RestAdapter.Builder()
                .setEndpoint("http://45.221.74.26/bio-api/")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiService = registrationAdapter.create(ApiService.class);
        takeIdPhotoBtn = findViewById(R.id.take_id_photo_btn);
        enrollIdPhoto = findViewById(R.id.enroll_id_photo);
        submitBtn = findViewById(R.id.submit_btn);
        user = new User();
        telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        takeIdPhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
                File f = new File(Environment.getExternalStorageDirectory(),
                        "photo.jpg");
                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                mUri = Uri.fromFile(f);
                startActivityForResult(i, TAKE_PHOTO_ID);
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Sending information");
                progressDialog.show();

                apiService.registerUser(
                        Utils.getPhone(EnrollPartFourActivity.this),
                        Utils.getFirstName(EnrollPartFourActivity.this),
                        Utils.getLastName(EnrollPartFourActivity.this),
                        Utils.getPin(EnrollPartFourActivity.this),
                        Utils.getGender(EnrollPartFourActivity.this),
                        telephonyManager.getDeviceId(),
                        Utils.getNationalId(EnrollPartFourActivity.this),
                        Utils.getDob(EnrollPartFourActivity.this),
                        Utils.getEmail(EnrollPartFourActivity.this),
                        Utils.getCity(EnrollPartFourActivity.this),
                        Utils.getNokName(EnrollPartFourActivity.this),
                        Utils.getNokPhone(EnrollPartFourActivity.this),
                        "testagent",
                        "1213",
                        "face",
                        "",
                        new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        progressDialog.dismiss();
                        Crouton.makeText(EnrollPartFourActivity.this, user.getResult(), Style.INFO).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressDialog.dismiss();
                        Crouton.makeText(EnrollPartFourActivity.this, "AN ERROR OCCURRED", Style.ALERT).show();
                    }
                });
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if (data != null) {
        switch (requestCode) {
            case TAKE_PHOTO_ID:
                System.out.println("Take picture result"+ Activity.RESULT_OK);
                if (resultCode == Activity.RESULT_OK) {
                    getContentResolver().notifyChange(mUri, null);
                    ContentResolver cr = getContentResolver();
                    try {
                        mPhoto = android.provider.MediaStore.Images.Media
                                .getBitmap(cr, mUri);
                        enrollIdPhoto.setImageBitmap(mPhoto);
                    } catch (Exception e) {
                        System.out.println("Fails"+e.getMessage());
                    }
                }

        }

    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), EnrollPartThreeActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }
}
