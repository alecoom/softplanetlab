package ug.co.ahurire.banywanibaitu;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class EnrollPartThreeActivity extends AppCompatActivity {
    Button goToPart4Btn, enrollTakePhotoIdBtn;
    private Uri mUri;
    private Bitmap mPhoto;
    private static final int TAKE_PICTURE = 0;
    ImageView enrollCustPhoto;
    Context context = this;
    private static final int REQUEST_READ_PHONE_STATE = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll_part_three);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(EnrollPartThreeActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        }

        goToPart4Btn = findViewById(R.id.goToPart4_btn);
        enrollTakePhotoIdBtn = findViewById(R.id.enroll_take_photoId_btn);
        enrollCustPhoto = findViewById(R.id.enroll_custPhoto);
        goToPart4Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EnrollPartThreeActivity.this, EnrollPartFourActivity.class);
                startActivity(i);
            }
        });

        enrollTakePhotoIdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
                File f = new File(Environment.getExternalStorageDirectory(),
                        "photo.jpg");
                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                mUri = Uri.fromFile(f);
                startActivityForResult(i, TAKE_PICTURE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if (data != null) {
        switch (requestCode) {
            case TAKE_PICTURE:
                System.out.println("Take picture result"+Activity.RESULT_OK);
                if (resultCode == Activity.RESULT_OK) {
                    getContentResolver().notifyChange(mUri, null);
                    ContentResolver cr = getContentResolver();
                    try {
                        mPhoto = android.provider.MediaStore.Images.Media
                                .getBitmap(cr, mUri);
                         enrollCustPhoto.setImageBitmap(mPhoto);
                    } catch (Exception e) {
                        System.out.println("Fails"+e.getMessage());
                    }
                }

        }

    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), EnrollPartTwoActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }
}
