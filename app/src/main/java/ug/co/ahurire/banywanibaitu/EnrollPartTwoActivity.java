package ug.co.ahurire.banywanibaitu;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;

import ug.co.ahurire.banywanibaitu.utils.Alerter;
import ug.co.ahurire.banywanibaitu.utils.NumberTextWatcher;
import ug.co.ahurire.banywanibaitu.utils.Utils;

public class EnrollPartTwoActivity extends AppCompatActivity {
    Button goToPart3Btn, dobBtn;
    Spinner parishSpinner, prayerGroupSpinner, paymentScheduleSpinner;
    String dob;
    TextView dobText;
    EditText city, nextOfKinName, nextOfKinPhone, pledge;
    Alerter alerter;
    Context context = this;
    private static final int READ_EXTERNAL_STORAGE = 0;
    static final int DATE_DIALOG_ID = 0;
    public int monthSelected, daySelected;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String parishes[] = {"Select Parish","Kitamba", "Nkungu", "Nshunga", "Akayanja", "Kanyaryeru"};
    String prayer_groups[] = {"Select Prayer Group", "Prayer Group 1", "Prayer Group 2", "Prayer Group 3"};
    String payment_schedules[] = {"Select Payment Schedule", "Daily", "Weekly", "Fortnight", "Monthly"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll_part_two);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        parishSpinner = findViewById(R.id.parish_spinner);
        prayerGroupSpinner = findViewById(R.id.prayer_group_spinner);
        paymentScheduleSpinner = findViewById(R.id.payment_schedule_spinner);
        parishSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, parishes));
        prayerGroupSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, prayer_groups));
        paymentScheduleSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, payment_schedules));
        dobText = findViewById(R.id.enroll_dob_show);
        city = findViewById(R.id.enroll_city);
        nextOfKinName = findViewById(R.id.enroll_nokName);
        nextOfKinPhone = findViewById(R.id.enroll_nokPhone);
        goToPart3Btn = findViewById(R.id.goToPart3_btn);
        dobBtn = findViewById(R.id.dob_btn);
        pledge = findViewById(R.id.pledge);
        pledge.addTextChangedListener(new NumberTextWatcher(pledge));
        alerter = new Alerter(this);
        int storagePermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (storagePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(EnrollPartTwoActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE);
        }

        dobBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        goToPart3Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (city.getText().toString().isEmpty()) {
                    alerter.alerterError("City is required");
                } else if (nextOfKinName.getText().toString().isEmpty()) {
                    alerter.alerterError("Next of Kin Name is required");
                } else if (nextOfKinPhone.getText().toString().isEmpty()) {
                    alerter.alerterError("Next of Kin Phone is required");
                } else {
                    Utils.saveDob(dobText.getText().toString(), EnrollPartTwoActivity.this);
                    Utils.saveParish(parishes[parishSpinner.getSelectedItemPosition()], EnrollPartTwoActivity.this);
                    Utils.savePrayerGroup(prayer_groups[prayerGroupSpinner.getSelectedItemPosition()], EnrollPartTwoActivity.this);
                    Utils.savePaymentSchedule(payment_schedules[paymentScheduleSpinner.getSelectedItemPosition()], EnrollPartTwoActivity.this);
                    Utils.savePledge(pledge.getText().toString().replace(",", ""), EnrollPartTwoActivity.this);
                    Utils.saveCity(city.getText().toString(), EnrollPartTwoActivity.this);
                    Utils.saveNextOfKinName(nextOfKinName.getText().toString(), EnrollPartTwoActivity.this);
                    Utils.saveNextOfKinPhone(nextOfKinPhone.getText().toString(), EnrollPartTwoActivity.this);
                    Intent i = new Intent(EnrollPartTwoActivity.this, EnrollPartThreeActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() { // the

        public void onDateSet(DatePicker view, int yearSelected,
                              int monthOfYear, int dayOfMonth) {
            System.out.println("date dialog called: year" + yearSelected
                    + " month:" + monthOfYear + " day:" + dayOfMonth);
            monthSelected = monthOfYear;
            daySelected = dayOfMonth;
            dob = yearSelected + "-" + (monthSelected + 1) + "-" + daySelected;

            TextView reorderDate = findViewById(R.id.enroll_dob_show);
            reorderDate.setText(dob);
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                System.out.println("DATE_DIALOG_ID selected");
                System.out.println("date dialog called:" + mYear + ":" + mMonth);
                final Calendar c = Calendar.getInstance();
                System.out.println("creating calendar");
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
                        mDay);
        }
        return null;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }
}
