package ug.co.ahurire.banywanibaitu;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.JsonObject;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ug.co.ahurire.banywanibaitu.models.PaymentResponse;
import ug.co.ahurire.banywanibaitu.services.ApiService;
import ug.co.ahurire.banywanibaitu.utils.NumberTextWatcher;

public class MainActivity extends AppCompatActivity {

    Button registerBtn, payPledgeBtn, saveBtn;
    private ApiService mmPaymentService;
    RestAdapter mmPaymentAdapter;
    private ProgressDialog progressDialog;
    String paymentMode, auth;
    CoordinatorLayout coordinatorLayout;
    private final static Integer TRANS_TYPE_ID = 43;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        registerBtn = findViewById(R.id.register_btn);
        payPledgeBtn = findViewById(R.id.pay_pledge_btn);
        saveBtn = findViewById(R.id.save_btn);
        coordinatorLayout = findViewById(R.id.coordinate_layout);
        mmPaymentAdapter = new RestAdapter.Builder()
                .setEndpoint("https://mcash.ug/trueafrican/ussd")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        mmPaymentService = mmPaymentAdapter.create(ApiService.class);
        progressDialog = new ProgressDialog(this);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
            }
        });
        payPledgeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.mm_payment_layout, null);
                dialogBuilder.setView(dialogView);
                final Button mmPayBtn = dialogView.findViewById(R.id.mm_pay_btn);
                final Button mmCancelBtn = dialogView.findViewById(R.id.mm_cancel_btn);
                final EditText forAccountNumber = dialogView.findViewById(R.id.mm_for_account_number);
                final EditText fromAccountNumber = dialogView.findViewById(R.id.mm_from_account_number);
                final EditText amount = dialogView.findViewById(R.id.mm_amout);
                amount.addTextChangedListener(new NumberTextWatcher(amount));
                final AlertDialog mmPayDialog = dialogBuilder.create();
                mmPayDialog.show();

                //MM Cash Btn
                mmPayBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JsonObject mmPayment = new JsonObject();
                        mmPayment.addProperty("action", "deposit");
                        mmPayment.addProperty("msisdn", forAccountNumber.getText().toString());
                        mmPayment.addProperty("mm_number", fromAccountNumber.getText().toString());
                        mmPayment.addProperty("txnTypeId",TRANS_TYPE_ID);
                        mmPayment.addProperty("destination", "256772415497");
                        mmPayment.addProperty("amount", amount.getText().toString().replace(",", ""));
                        if(amount.getText().toString().isEmpty()) {
                            Crouton.makeText(MainActivity.this, "Amount is Required", Style.ALERT).show();
                        } else if (forAccountNumber.getText().toString().isEmpty()) {
                            Crouton.makeText(MainActivity.this, "For Account is required", Style.ALERT).show();
                        } else if (fromAccountNumber.getText().toString().isEmpty()) {
                            Crouton.makeText(MainActivity.this, "From Account is Required", Style.ALERT).show();
                        } else {
                            makeMMPayment(mmPayment, mmPayDialog);
                        }
                    }
                });

                //MM Cancel Btn
                mmCancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mmPayDialog.dismiss();
                    }
                });
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.mm_save_layout, null);
                dialogBuilder.setView(dialogView);
                final Button saccoPayBtn = dialogView.findViewById(R.id.sacco_pay_btn);
                final Button saccoCancelBtn = dialogView.findViewById(R.id.sacco_cancel_btn);
                final EditText saccoAccount = dialogView.findViewById(R.id.sacco_account);
                final EditText saccoFromAccountNumber = dialogView.findViewById(R.id.sacco_mm_from_account_number);
                final EditText amount = dialogView.findViewById(R.id.sacco_mm_amout);
                amount.addTextChangedListener(new NumberTextWatcher(amount));
                final AlertDialog mmPayDialog = dialogBuilder.create();
                mmPayDialog.show();

                //MM Cash Btn
                saccoPayBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JsonObject mmPayment = new JsonObject();
                        mmPayment.addProperty("action", "deposit");
                        mmPayment.addProperty("msisdn", saccoFromAccountNumber.getText().toString());
                        mmPayment.addProperty("mm_number", saccoFromAccountNumber.getText().toString());
                        mmPayment.addProperty("txnTypeId", TRANS_TYPE_ID);
                        mmPayment.addProperty("destination", "agapeyouth");
                        mmPayment.addProperty("amount", amount.getText().toString().replace(",", ""));
                        if(amount.getText().toString().isEmpty()) {
                            Crouton.makeText(MainActivity.this, "Amount is Required", Style.ALERT).show();
                        } else if (saccoAccount.getText().toString().isEmpty()) {
                            Crouton.makeText(MainActivity.this, "Please input Sacco Account", Style.ALERT).show();
                        } else if (saccoFromAccountNumber.getText().toString().isEmpty()) {
                            Crouton.makeText(MainActivity.this, "Your Mobile Money account is Required", Style.ALERT).show();
                        } else {
                            makeMMPayment(mmPayment, mmPayDialog);
                        }

                    }
                });

                //MM Cancel Btn
                saccoCancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mmPayDialog.dismiss();
                    }
                });
            }
        });
    }

    public void showMessage(String message){
        Snackbar.make(coordinatorLayout,message,Snackbar.LENGTH_LONG).show();
    }

    public void showError(String message){
        Snackbar.make(coordinatorLayout, message,Snackbar.LENGTH_LONG).show();
    }

    public void makeMMPayment(JsonObject mmPayment, final Dialog dialog){
        progressDialog.setTitle("Mobile Money Payment");
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Initiating a Mobile Money transaction");
        progressDialog.show();
        paymentMode = "MM_PAYMENT";
        mmPaymentService.payWithMM(auth,mmPayment,new PaymentCallback(){
            @Override
            public void success(PaymentResponse paymentResponse, Response response) {
//                Log.e("SUCCESS", fuel.getName());
                System.out.println("---- Response From Server -----");
                progressDialog.dismiss();
                dialog.dismiss();
                Crouton.makeText(MainActivity.this, paymentResponse.getMessage(), Style.INFO).show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                dialog.dismiss();
                Crouton.makeText(MainActivity.this, error.getMessage(), Style.ALERT).show();

            }
        });
    }

    private class PaymentCallback implements Callback<PaymentResponse> {
        @Override
        public void success(PaymentResponse paymentResponse, Response response) {
            if(progressDialog.isShowing()){ progressDialog.dismiss();}
            if(!paymentResponse.getStatus().equalsIgnoreCase("DENIED")){
                //sendSalesInformation(distributor, retailer);
                showMessage(String.format("Payment was Successful"));

            }else{

                showError(String.format("An Error occurred %s %s", paymentResponse.getStatus(),
                        paymentResponse.getMessage()));
            }
        }

        @Override
        public void failure(RetrofitError error) {
            if(progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            showError(String.format("Payment failed because of %s ", error.getMessage()));
        }
    }
}
