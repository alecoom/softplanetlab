package ug.co.ahurire.banywanibaitu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import ug.co.ahurire.banywanibaitu.utils.Alerter;
import ug.co.ahurire.banywanibaitu.utils.Utils;

public class RegisterActivity extends AppCompatActivity {
    Button goToPart2Btn;
    EditText firstName, lastName, phone, email, pin, confirmPin, nationalId;
    Alerter alerter;
    Spinner sexSpinner;
    String sexes [] = {"Male", "Female"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        phone = findViewById(R.id.phone);
        email = findViewById(R.id.email);
        sexSpinner = findViewById(R.id.sex_spinner);
        sexSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, sexes));
        pin = findViewById(R.id.pin);
        confirmPin = findViewById(R.id.confirm_pin);
        nationalId = findViewById(R.id.national_id);
        alerter = new Alerter(this);

        goToPart2Btn = findViewById(R.id.gotToPart2_btn);
        goToPart2Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firstName.getText().toString().isEmpty()) {
                    alerter.alerterError("First Name is required.");
                } else if (lastName.getText().toString().isEmpty()){
                    alerter.alerterError("Last Name is required.");
                } else if (phone.getText().toString().isEmpty()) {
                    alerter.alerterError("Phone is required.");
                } else if (email.getText().toString().isEmpty()) {
                    alerter.alerterError("Email is required");
                } else if (!isEmailValid(email.getText().toString())) {
                    alerter.alerterError("Invalid Email");
                } else if (pin.getText().toString().isEmpty()) {
                    alerter.alerterError("Pin is required");
                } else if (confirmPin.getText().toString().isEmpty()){
                    alerter.alerterError("Please Confirm Pin");
                } else if (!pin.getText().toString().equals(confirmPin.getText().toString())) {
                    alerter.alerterError("Pin and Confirm Pin must be equal");
                } else if (nationalId.getText().toString().isEmpty()) {
                    alerter.alerterError("National ID is required.");
                } else {

                    Utils.saveFirstName(firstName.getText().toString(), RegisterActivity.this);
                    Utils.saveLastName(lastName.getText().toString(), RegisterActivity.this);
                    Utils.savePhone(phone.getText().toString(), RegisterActivity.this);
                    Utils.saveEmail(email.getText().toString(), RegisterActivity.this);
                    Utils.savePin(pin.getText().toString(), RegisterActivity.this);
                    Utils.saveNationId(nationalId.getText().toString(), RegisterActivity.this);
                    Utils.saveGender(sexes[sexSpinner.getSelectedItemPosition()], RegisterActivity.this);

                    Intent i = new Intent(RegisterActivity.this, EnrollPartTwoActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }
}
