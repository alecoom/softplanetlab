package ug.co.ahurire.banywanibaitu.models;


public class PaymentResponse {
    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
