package ug.co.ahurire.banywanibaitu.services;


import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Header;
import retrofit.http.POST;
import ug.co.ahurire.banywanibaitu.models.PaymentResponse;
import ug.co.ahurire.banywanibaitu.models.User;

public interface ApiService {

    @FormUrlEncoded
    @POST("/testFile.php")
    void registerUser(
            @Field("mobile") String mobile,
            @Field("firstName") String firstName,
            @Field("lastName") String lastName,
            @Field("pinCode") String pinCode,
            @Field("gender") String gender,
            @Field("imei") String imei,
            @Field("idNo") String idNo,
            @Field("dob") String dob,
            @Field("email") String email,
            @Field("city") String city,
            @Field("nokName") String nokName,
            @Field("nokPhone") String nokPhone,
            @Field("agentNumber") String agentNumber,
            @Field("agentPin") String agentPin,
            @Field("phototype") String phototype,
            @Field("photoData") String photoData, Callback<User> ur);

    @POST("/incoming.php")
    void payWithMM(@Header("Authorization") String authorization,
                   @Body JsonObject mmPayment,
                   Callback<PaymentResponse> response);

}
