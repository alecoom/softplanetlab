package ug.co.ahurire.banywanibaitu.utils;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import ug.co.ahurire.banywanibaitu.MainActivity;
import ug.co.ahurire.banywanibaitu.R;
import ug.co.ahurire.banywanibaitu.RegisterActivity;

public class Alerter {

    //Shared prefs
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    TextView serverText;
    AlertDialog levelDialog;

    CharSequence[] serverList = null;

    /**
     * @param args
     */
    Context context;

    public Alerter(Context context) {
        this.context = context;
    }


    //----------------------------------------------------------------------------
    // ALERT GENERATOR
    //public void alerter(String title, String message,Context context) {
    public void alerter(String title, String message, Context context) {


        // public void onClick(View arg0) {
        // loadEm();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        // MainActivity.this.finish();


                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        // }
    }

    //CANCEL DIALOG CLIENT FUNCTIONS
    public void alerterConfirmCancelClient(String message){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView;
        promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

        TextView errorTxt = promptsView.findViewById(R.id.confirm_cancel_text);

        // errorTxt.setText("Are you sure you want to cancel?");
        errorTxt.setText(message);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //dialog.cancel();
                                Intent i = new Intent();
                                i.setClass(context, RegisterActivity.class);
                                context.startActivity(i);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    //CANCEL DIALOG AGENT FUNCTIONS
    public void alerterConfirmCancel(String message){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView;
        promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

        // TextView accountTxt = (TextView) promptsView.findViewById(R.id.confirm_dialog_acct);
        TextView errorTxt = promptsView.findViewById(R.id.confirm_cancel_text);

        // errorTxt.setText("Are you sure you want to cancel?");
        errorTxt.setText(message);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //dialog.cancel();
                                Intent i = new Intent();
                                i.setClass(context, MainActivity.class);
                                context.startActivity(i);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    public void alerterError(String message){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView;
        promptsView = li.inflate(R.layout.error_dialog, null);

        // TextView accountTxt = (TextView) promptsView.findViewById(R.id.confirm_dialog_acct);
        TextView errorTxt = promptsView.findViewById(R.id.error_dialog_error);

        errorTxt.setText(message);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //dialog.cancel();
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    public void alerterBadFingerProceed(String message){
        System.out.println("alerterBadFingerProceed");
        System.out.println("message:"+message);
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView;
        promptsView = li.inflate(R.layout.error_dialog, null);

        // TextView accountTxt = (TextView) promptsView.findViewById(R.id.confirm_dialog_acct);
        TextView messageTxt = promptsView.findViewById(R.id.error_dialog_error);

        messageTxt.setText(message);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //dialog.cancel();
                                dialog.cancel();
//				    	Intent i = new Intent();
//						i.setClass(context, Agent_home.class);
//						context.startActivity(i);
                            }
                        }).setNegativeButton("SKIP",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        alerterSuccessSimple("Finger print added, enrollment compelete");
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }

    public void alerterSuccessNoHome(String message){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView;
        promptsView = li.inflate(R.layout.dialog_success_simple, null);

        // TextView accountTxt = (TextView) promptsView.findViewById(R.id.confirm_dialog_acct);
        TextView messageTxt = promptsView.findViewById(R.id.success_simple_message);

        messageTxt.setText(message);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //dialog.cancel();
                                dialog.cancel();
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void alerterSuccessSimple(String message){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView;
        promptsView = li.inflate(R.layout.dialog_success_simple, null);

        // TextView accountTxt = (TextView) promptsView.findViewById(R.id.confirm_dialog_acct);
        TextView messageTxt = promptsView.findViewById(R.id.success_simple_message);

        messageTxt.setText(message);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //dialog.cancel();
                                //dialog.cancel();
                                Intent i = new Intent();
                                i.setClass(context, MainActivity.class);
                                context.startActivity(i);
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void alerterSuccess(String account, String amount, String conf){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView;
        promptsView = li.inflate(R.layout.dialog_success, null);

        // TextView accountTxt = (TextView) promptsView.findViewById(R.id.confirm_dialog_acct);
        TextView accountTxt = promptsView.findViewById(R.id.success_dialog_acct);
        TextView amountTxt = promptsView.findViewById(R.id.success_dialog_amount);
        TextView confTxt = promptsView.findViewById(R.id.success_dialog_conf);

        accountTxt.setText(account);
        amountTxt.setText(amount);
        confTxt.setText(conf);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //dialog.cancel();
                                //dialog.cancel();
                                Intent i = new Intent();
                                i.setClass(context, MainActivity.class);
                                context.startActivity(i);
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    void log(String msg){
        System.out.println(msg);
    }

}

