package ug.co.ahurire.banywanibaitu.utils;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Utils {
    public static final String MyPREFERENCES = "userInfo";

    public static SharedPreferences getSharedPreferencesWithin(Activity givenActivity) {
        return givenActivity.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    }

    public static void saveFirstName(String firstName, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("first_name", firstName).commit();
    }

    public static void saveLastName(String lastName, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("last_name", lastName).commit();
    }

    public static void savePhone(String phone, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("phone", phone).commit();
    }

    public static void saveEmail(String email, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("email", email).commit();
    }

    public static void savePin(String pin, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("pin", pin).commit();
    }

    public static void saveNationId(String nationalId, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("national_id", nationalId).commit();
    }

    public static void saveDob(String dob, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("dob", dob).commit();
    }

    public static void saveParish(String parish, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("parish", parish).commit();
    }

    public static void savePrayerGroup(String prayerGroup, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("prayer_group", prayerGroup).commit();
    }

    public static void savePaymentSchedule(String paymentSchedule, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("payment_schedule", paymentSchedule).commit();
    }

    public static void savePledge(String pledge, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("pledge", pledge).commit();
    }

    public static void saveCity(String city, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("city", city).commit();
    }

    public static void saveNextOfKinName(String nextOfKinName, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("next_of_kin_name", nextOfKinName).commit();
    }

    public static void saveNextOfKinPhone(String nextOfKinPhone, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("next_of_kin_phone", nextOfKinPhone).commit();
    }

    public static void saveUserPhoto(String userPhoto, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("user_photo", userPhoto).commit();
    }

    public static void saveIdPhoto(String idPhoto, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("id_photo", idPhoto).commit();
    }

    public static void saveGender(String gender, Activity givenActivity) {
        SharedPreferences prefs = getSharedPreferencesWithin(givenActivity);
        prefs.edit().putString("gender", gender).commit();
    }

    public static String getFirstName(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("first_name", "");
    }

    public static String getLastName(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("last_name","");
    }

    public static String getPhone(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("phone","");
    }

    public static String getEmail(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        String email = prefs.getString("email", "");
        return email;
    }

    public static String getPin(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("pin", "");
    }

    public static String getNationalId(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("national_id", "");
    }

    public static String getDob(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("dob", "");
    }

    public static String getParish(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("parish", "");
    }

    public static String getPrayerGroup(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        String prayerGroup = prefs.getString("prayer_group", "");
        return prayerGroup;
    }

    public static String getPaymentSchedule(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("payment_schedule", "");
    }

    public static String getPledge(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("pledge", "");
    }

    public static String getCity(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("city", "");
    }

    public static String getNokName(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("next_of_kin_name", "");
    }

    public static String getNokPhone(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("next_of_kin_phone", "");
    }

    public static String getUserPhoto(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("user_photo", "");
    }

    public static String getIdPhoto(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("id_photo", "");
    }

    public static String getGender(Activity activity) {
        SharedPreferences prefs = getSharedPreferencesWithin(activity);
        return prefs.getString("gender", "");
    }
}
